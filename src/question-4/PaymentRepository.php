<?php

namespace App\Repositories;

use App\Models\Payment;

class PaymentRepository
{
    public function getAll(array $reqOptions): ?array
    {
        // $payments = Payment::all(); // table with roughly 1.2m records
        // $return = [];

        // if (!count($payments)) {
        //     return $return;
        // }

        // if ($reqOptions['sortDir'] === 'desc') {
        //     $payments = $payments->sortByDesc('id');
        // }

        // for ($i = 0; $i < $reqOptions['limit'] ?? 20; $i++) {
        //     $pmt = $payments[$i]->toArray();

        //     $return[] = array_merge($pmt, [
        //         'facility' => $payments[$i]->facility,
        //         'user' => $payments[$i]->user,
        //     ]);
        // }



        // The bottleneck here is that are the payments are retrieved and then for each payment retrieved,
        // they are run through another for loop to get the facility and user.  Creating the n+1 query 
        // problem.  To fix, can use eager loading and force the ORM to perform a joi and return all the
        // data in one single query


        $payments = Payment::all()->with('facility', 'user')->get(); // table with roughly 1.2m records
        $return = [];

        if (!count($payments)) {
            return $return;
        }

        if ($reqOptions['sortDir'] === 'desc') {
            $return = $payments->sortByDesc('id');
        }

        return $return;
    }
}
