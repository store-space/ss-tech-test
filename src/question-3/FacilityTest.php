<?php

namespace Test\Unit;

use Facility;
use Test\TestCase;


class FacilityTest extends TestCase
{
    public function setUp()
    {
        $this->facility = new Facility;
        $this->facility = [
            'id' => '1234',
            'location_code' => '2345',
            'name' => "123 Hideaway Lane",
            'street_address' => "Anywhere",
            'city' => "Anycity",
            'county' => "USA",
            'formatted_county' => "country",
            'state' => "State",
            'postal_code' => "12345-4321",
            'longitude' => "-104.990250",
            'latitude' =>  "39.739235",
            'phone' => "123-456-7890",
            'contact_email' =>  "test@test.com",
            'bre_group_id' => "54321"
        ];
    }

    public function testGetId()
    {
        $response = $this->get('/getId');
        $response->assertContains("1234", $id, "Does not contain value");
        $response->assertStatus('200');
    }

    public function testSetId()
    {
        $response = $this->post($this->facility->id);
        $response->assertStatus('200');
    }

    public function testGetLocationCode()
    {
        $response = $this->get('/getLocationCode');
        $response->assertStatus('200');
    }

    public function testSetLocationCode()
    {
        $response = $this->post($this->facility->location_code);
        $response->assertStatus('200');
    }

    public function testGetName()
    {
        $response = $this->get('/getName');
        $response->assertStatus('200');
    }

    public function testSetName()
    {
        $response = $this->post($this->facility->name);
        $response->assertStatus('200');
    }
       

    public function testGetStreetAddress()
    {
        $response = $this->get('/getStreetAddress');
        $response->assertStatus('200');
    }

    public function testSetStreetAddress()
    {
        $response = $this->post($this->facility->street_address);
        $response->assertStatus('200');
    }

    public function testGetCity()
    {
        $response = $this->get('/getCity');
        $response->assertStatus('200');
    }

    public function testSetCity()
    {
        $response = $this->post($this->facility->city);
    }

    public function testGetCounty()
    {
        $response = $this->get('/getCounty');
        $response->assertStatus('200');
    }

    public function testSetCounty()
    {
        $response = $this->post($this->facility->county);
    }

    public function testGetFormattedCounty()
    {
        $response = $this->get('/getFormattedCounty');
        $response->assertStatus('200');
    }

    public function testSetFormattedCounty()
    {
        $response = $this->post($this->facility->formatted_county);
    }

    public function testGetState()
    {
        $response = $this->get('/getState');
        $response->assertStatus('200');
    }

    public function testSetState()
    {
        $response = $this->post($this->facility->state);
    }

    public function testGetPostalCode()
    {
        $response = $this->get('/getPostalCode');
        $response->assertStatus('200');
    }

    public function testSetPostalCode()
    {
        $response = $this->post($this->facility->postal_code);
    }

    public function testGetLatitude()
    {
        $response = $this->get('/getLatitude');
        $response->assertStatus('200');
    }

    public function testSetLatitude()
    {
        $response = $this->post($this->facility->latitude);
    }

    public function testGetLongitude()
    {
        $response = $this->get('/getLongitude');
        $response->assertStatus('200');
    }

    public function testSetLongitude()
    {
        $response = $this->post($this->facility->longitude);
    }

    public function testGetPhone()
    {
        $response = $this->get('/getPhone');
        $response->assertStatus('200');
    }

    public function testSetPhone()
    {
        $response = $this->post($this->facility->phone);
    }

    public function testGetContactEmail()
    {
        $response = $this->get('/getContactEmail');
        $response->assertStatus('200');
    }

    public function testSetContactEmail()
    {
        $response = $this->post($this->facility->contact_email);
    }

    public function testGetBreGroupId()
    {
        $response = $this->get('/getBreGroupId');
        $response->assertStatus('200');
    }

    public function testSetBreGroupId()
    {
        $response = $this->post($this->facility->bre_group_id);
    }
       
}